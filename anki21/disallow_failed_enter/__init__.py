from aqt.reviewer import Reviewer
from aqt.utils import tooltip
from anki.utils import stripHTML
import re
import html.parser

def on_answer_button_patched(self, val):
	if self.typedAnswer != None:
		parser = html.parser.HTMLParser()
		try:
			cor = self.mw.col.media.strip(self.typeCorrect)
		except:
			return on_answer_button_unpatched(self, val)
		cor = re.sub("(\n|<br ?/?>|</?div>)+", " ", cor)
		cor = stripHTML(cor)
		# ensure we don't chomp multiple whitespace
		cor = cor.replace(" ", "&nbsp;")
		cor = parser.unescape(cor)
		cor = cor.replace("\xa0", " ")
		cor = cor.strip()
		given = self.typedAnswer
		if cor == given:
			self._answerCard(self._defaultEase())
		else:
			tooltip("Typing incorrect: please manually specify difficulty.")
	else:
		on_answer_button_unpatched(self, val)

on_answer_button_unpatched = Reviewer._onAnswerButton
Reviewer._onAnswerButton = on_answer_button_patched

