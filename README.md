# eta's Anki addons

This repository stores [eta](https://theta.eu.org)'s custom addons for
the [Anki](https://apps.ankiweb.net/) spaced-repetition flashcard system.

Feedback should be addressed to any contact method in the section
at the bottom of [theta.eu.org](https://theta.eu.org/).

## Anki 2.0

- [`burnrate.py`](https://git.theta.eu.org/anki-addons.git/tree/anki20/burnrate.py): displays the number of days required
  to learn all of the new cards in a given deck, at the current rate of learning
- [`Disallow_Failed_Enter.py`](https://git.theta.eu.org/anki-addons.git/tree/anki20/Disallow_Failed_Enter.py): prevents
  you from pressing `Enter` to mark a card as correct, if type-in-the-answer is enabled and the answer you typed was
  incorrect (instead, you must manually press 1, 2, 3 or 4).

## Anki 2.1

- **Disable Fade** ([ankiweb](https://ankiweb.net/shared/info/1533933651)): disables the 100ms fade transition / animation
  between cards in Anki 2.1, which is subjectively annoying as hell
- **Disallow failed enter** ([ankiweb](https://ankiweb.net/shared/info/1492767191)): does the same thing as the 2.0 version,
  except ported for 2.1 (see above).
